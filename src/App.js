import avatar from './assets/images/48.jpg'
const CSS = {
  devCamp :{
    margin: "auto",
    padding: "0 20em"
  },
  devcampWrapper : {
    textAlign: "center",
    maxWidth: "950px",
    margin: "50px auto 0px",
    border: "1px solid #e6e6e6",
    padding: "40px 25px",
    backgroundColor: "bisque"

  },
  devcampQuoteP : {
    lineHeight: "1.5",
    fontWeight: "300",
    marginBottom: "25px",
    fontSize: "1.375rem",
    fontStyle:"italic"
  },
  devcampAvatar : {
    margin: "-90px auto 30px",
    width: "100px",
    borderRadius: "50%",
    objectFit: "cover",
    marginBottom: "0"
  },
  devcampName : {
    marginBottom: "0",
    fontWeight: "600",
    fontSize: "1rem",
    color: "blue"
  },
  devcampNameSpan : {
    fontWeight : "400",
    color: "brown"
  }
}
function App() {
  return (
    <div style={CSS.devCamp}>
      <div style={CSS.devcampWrapper}><img alt='avatar' style={CSS.devcampAvatar} src={avatar} />
        <div>
          <p  style={CSS.devcampQuoteP}>This is one of the best developer blogs on the plannet. I read it daily to improve my skills </p>
        </div>
        <p style={CSS.devcampName}>Tammy Stevens.<span style={CSS.devcampNameSpan}>- Front end developer</span></p></div>
    </div>
  );
}

export default App;
